package com.example.tuoxi.test20160402;

import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.maps.OnStreetViewPanoramaReadyCallback;
import com.google.android.gms.maps.StreetViewPanorama;
import com.google.android.gms.maps.StreetViewPanoramaFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.StreetViewPanoramaCamera;

import java.util.List;

import static java.lang.Math.cos;
import static java.lang.Math.sin;
import static java.lang.StrictMath.sqrt;

public class StreetViewActivity extends AppCompatActivity implements OnStreetViewPanoramaReadyCallback, SensorEventListener {

    private static final double EPSILON = 0.00001;
    private float streeX;
    private float streeY;

    private StreetViewPanorama str;

    private SensorManager mSensorManager;
    private Sensor mSensor;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent testFromMain = getIntent();
        streeX = testFromMain.getFloatExtra("streeX", 0);
        streeY = testFromMain.getFloatExtra("streeY", 0);

        setContentView(R.layout.activity_street_view);
        StreetViewPanoramaFragment streetViewPanoramaFragment =
                (StreetViewPanoramaFragment) getFragmentManager()
                        .findFragmentById(R.id.streetviewpanorama);
        streetViewPanoramaFragment.getStreetViewPanoramaAsync(this);

        Button buttonAni = (Button) findViewById(R.id.streetviewbutton);

        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        mSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);

        buttonAni.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Set the tilt to zero, keeping the zoom and bearing at current values.
                // Animate over a duration of 500 milliseconds.
                long duration = 3000;
                float tilt = 0;
                StreetViewPanoramaCamera camera = new StreetViewPanoramaCamera.Builder()
                        .zoom(str.getPanoramaCamera().zoom)
                        .bearing(str.getPanoramaCamera().bearing)
                        .tilt(tilt)
                        .build();

                str.animateTo(camera, duration);
            }
        });

    }

    @Override
    public void onStreetViewPanoramaReady(StreetViewPanorama streetViewPanorama) {
        streetViewPanorama.setPosition(new LatLng(streeX, streeY));
        str = streetViewPanorama;
    }

    // Create a constant to convert nanoseconds to seconds.
    private static final float NS2S = 1.0f / 1000000000.0f;
    private final float[] deltaRotationVector = new float[4];
    private float timestamp;

    @Override
    protected void onResume() {
        super.onResume();

        {
            List<Sensor> sensors = mSensorManager.getSensorList(Sensor.TYPE_GYROSCOPE);

            if (sensors.size() > 0) {
                Sensor sensor = sensors.get(0);

                mSensorManager.registerListener(this,sensor,SensorManager.SENSOR_DELAY_UI);
            }
        }

    }

    @Override
    protected void onStop() {
        super.onStop();
        mSensorManager.unregisterListener(this);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {

        // This timestep's delta rotation to be multiplied by the current rotation
        // after computing it from the gyro sample data.
        if (timestamp != 0) {
            final float dT = (event.timestamp - timestamp) * NS2S;
            // Axis of the rotation sample, not normalized yet.
            float axisX = event.values[0];
            float axisY = event.values[1];
            float axisZ = event.values[2];

            Log.wtf("gyroscope", String.valueOf(axisX));

            // Calculate the angular speed of the sample
            float omegaMagnitude = (float) sqrt(axisX*axisX + axisY*axisY + axisZ*axisZ);

            // Normalize the rotation vector if it's big enough to get the axis
            // (that is, EPSILON should represent your maximum allowable margin of error)
            if (omegaMagnitude > EPSILON) {
                axisX /= omegaMagnitude;
                axisY /= omegaMagnitude;
                axisZ /= omegaMagnitude;
            }

            // Integrate around this axis with the angular speed by the timestep
            // in order to get a delta rotation from this sample over the timestep
            // We will convert this axis-angle representation of the delta rotation
            // into a quaternion before turning it into the rotation matrix.
            float thetaOverTwo = omegaMagnitude * dT / 2.0f;
            float sinThetaOverTwo = (float) sin(thetaOverTwo);
            float cosThetaOverTwo = (float) cos(thetaOverTwo);
            deltaRotationVector[0] = sinThetaOverTwo * axisX;
            deltaRotationVector[1] = sinThetaOverTwo * axisY;
            deltaRotationVector[2] = sinThetaOverTwo * axisZ;
            deltaRotationVector[3] = cosThetaOverTwo;
        }
        timestamp = event.timestamp;
        float[] deltaRotationMatrix = new float[9];
        SensorManager.getRotationMatrixFromVector(deltaRotationMatrix, deltaRotationVector);
        // User code should concatenate the delta rotation we computed with the current rotation
        // in order to get the updated rotation.
        // rotationCurrent = rotationCurrent * deltaRotationMatrix;

        long duration = 0;
        float tilt = 0;
        float bearing = 0;
        bearing = (event.values[0]*100) % 340;
        tilt = (event.values[1]*100) % 80;
        tilt = tilt;
        StreetViewPanoramaCamera camera = new StreetViewPanoramaCamera.Builder()
                .zoom(str.getPanoramaCamera().zoom)
                .bearing(bearing)
                .tilt(tilt)
                .build();

        str.animateTo(camera, duration);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

}
